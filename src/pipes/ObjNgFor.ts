/*
 http://stackoverflow.com/questions/37046138/how-to-use-ngfor-with-object/37046743#37046743
 */

import {Pipe, PipeTransform} from "@angular/core";
@Pipe({ name: 'ObjNgFor',  pure: false })
export class ObjNgFor implements PipeTransform {
  transform(value: any, args: any[] = null): any {
    return Object.keys(value)//.map(key => value[key]);
  }
}
