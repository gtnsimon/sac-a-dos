import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {API} from "./API";

/*
  Generated class for the VoyageProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class VoyageProvider extends API {

  public API_KEY = "&key=AIzaSyBBPZNQALwsjghA1T3QZA9gtvE8eXPL-eE";

  constructor(public http: Http) {
    super(http);
    console.log('Hello VoyageProvider Provider');
  }

  getCurrentTravel(user_id:number) {
    let endpoint = 'users/' + user_id + '/travels/current';
    return this.get(endpoint);
  }

  getTravelStep(user_id:number) {
    let endpoint = 'users/' + user_id + '/travels/current/next-step';
    return this.get(endpoint);
  }

  getTip(step_id:number, excluded = []) {
    let endpoint = 'travels/steps/' + step_id + '/tip?excluded=' + excluded.join(',');
    return this.get(endpoint);
  }

  cancelStep(user_id:number) {
    let endpoint = 'users/' + user_id + '/travels/current/next-step';
    return this.delete(endpoint);
  }

  scanQR(user_id:number, data: string) {
    data = JSON.parse(data);
    console.log('scanQR', data);
    let endpoint = 'users/' + user_id + '/travels/current/valid-step';
    return this.post(endpoint, data);
  }

}
