import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {API} from "./API";

/*
  Generated class for the HostsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class HostsProvider extends API {

  constructor(public http: Http) {
    super(http);
  }

  getHostsNearBy(lat:number, lng:number) {
    let endpoint = 'hosts/nearby?at=' + lat + ',' + lng;
    return this.get(endpoint);
  }

}
