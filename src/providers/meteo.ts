import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the InfoTrafic provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Meteo {
    /**
     * Clé API Open Weather Map.
     */
    API_KEY = "&APPID=afb0961774a2be918178b782c8e4e61c";

    constructor(public http: Http) {
    }

    /**
     * Récupération de la météo via l'API Open Weather Map.
     * @return Promise
     */
    getWeatherByPos(lat,lon) {
        return this.http.get('http://api.openweathermap.org/data/2.5/weather?lang=fr&units=metric&lat='+lat+'&lon='+lon+this.API_KEY).map(data => data.json());
    }

}
