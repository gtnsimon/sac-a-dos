import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the InfoTrafic provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class InfoTrafic {

  constructor(public http: Http) {
  }

  /**
   * Récupération de l'info-trafic via l'API Vianavigo.
   * @param string Info de la ligne spécifiée
   * @return Promise
   */
  getInfoTrafic(line:string = "") {
    if(line !== "") {
      let data = this.http.get('https://lab.vianavigo.com/api_vn2/lines/' + line + '/news').map(res => res.json());
      return data;
    }
    let data = this.http.get('https://lab.vianavigo.com/api_vn2/traffic').map(res => res.json());
    return data;
  }

}
