import { Injectable } from '@angular/core';
import {API} from "./API";
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";
import {Storage} from "@ionic/storage";

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LoginProvider extends API {

  constructor(public http: Http) {
    super(http);
  }

  getUser(id) {
    let user = this.get('users/' + id);
    return user;
  }

  putUser(user): Observable<Response> {
    user.facebook_id = user.id;
    return this.put('users', user);
  }

}
