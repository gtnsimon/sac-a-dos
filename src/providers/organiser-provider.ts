import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {API} from "./API";

/*
  Generated class for the OrganiserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class OrganiserProvider extends API {

  constructor(public http: Http) {
    super(http);
  }

  putTravel(data: any) {
    return this.put('travels', data);
  }

}
