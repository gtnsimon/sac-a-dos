import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";

export class API {

  //API_URL: string = "http://192.168.0.30/METWEB/SACADOS/web/";
  API_URL: string = "https://api.lamarmelade.fr/";

  constructor(public http: Http) {
    console.info('API', 'loaded');
  }

  get(endpoint: string) {
    return this.http.get(this.API_URL + endpoint).map(res => res.json());
  }

  post(endpoint: string, data: any): Observable<Response> {
    let headers = new Headers;
    headers.append('Content-Type', 'application/json');
    let post = this.http.post(this.API_URL + endpoint, JSON.stringify(data), { headers: headers }).map(res => res.json());
    return post;
  }

  put(endpoint: string, data: any): Observable<Response> {
    let headers = new Headers;
    headers.append('Content-Type', 'application/json');
    let put = this.http.put(this.API_URL + endpoint.replace('.', '/'), JSON.stringify(data), { headers: headers }).map(res => res.json());
    return put;
  }

  delete(endpoint: string): Observable<Response> {
    let headers = new Headers;
    headers.append('Content-Type', 'application/json');
    let del = this.http.delete(this.API_URL + endpoint.replace('.', '/'), { headers: headers }).map(res => res.json());
    return del;
  }

}
