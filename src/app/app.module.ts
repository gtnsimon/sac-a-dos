import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler, LoadingController} from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage';
import { Facebook } from "@ionic-native/facebook";
import { LoginSlides } from "../pages/login-slides/login-slides";
import { InfoTrafic } from "../providers/info-trafic";
import { NewsShowMore } from "../pages/home/newsShowMore";
import { Geolocation } from '@ionic-native/geolocation';
import { Meteo } from "../providers/meteo";
import { OrganiserPage } from "../pages/organiser/organiser";
import { FormBuilder } from "@angular/forms";
import { LoginProvider } from "../providers/login-provider";
import { OrganiserProvider } from "../providers/organiser-provider";
import {AddInformationsUser} from "../pages/user/add-informations-user/add-informations-user";
import {AddInformationsLikesUser} from "../pages/user/add-informations-likes-user/add-informations-likes-user";
import {ProfileUser} from "../pages/user/profile-user/profile-user";
import {StepTwoOrganiser} from "../pages/step-two-organiser/step-two-organiser";
import {VoyageProvider} from "../providers/voyage-provider";
import {VoyageMap} from "../pages/voyage-map/voyage-map";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {InfoStep} from "../pages/voyage-map/info-step";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {VoyageChasse} from "../pages/voyage-chasse/voyage-chasse";
import {HotesMap} from "../pages/hotes-map/hotes-map";
import {StepOrHote} from "../pages/step-or-hote/step-or-hote";
import {HotesProfile} from "../pages/hotes-profile/hotes-profile";
import {HostsProvider} from "../providers/hosts-provider";
import {ObjNgFor} from "../pipes/ObjNgFor";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    TabsPage,
    OrganiserPage,
    StepTwoOrganiser,
    LoginSlides,
    AddInformationsUser,
    AddInformationsLikesUser,
    ProfileUser,
    VoyageMap,
    VoyageChasse,
    StepOrHote,
    HotesMap,
    HotesProfile,
    NewsShowMore,
    InfoStep,
    ObjNgFor
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    TabsPage,
    OrganiserPage,
    StepTwoOrganiser,
    LoginSlides,
    AddInformationsUser,
    AddInformationsLikesUser,
    ProfileUser,
    VoyageMap,
    VoyageChasse,
    StepOrHote,
    HotesMap,
    HotesProfile,
    NewsShowMore,
    InfoStep
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativeStorage,
    Facebook,
    LocalNotifications,
    InfoTrafic,
    Geolocation,
    FormBuilder,
    BarcodeScanner,
    LoadingController,
    Meteo,
    LoginProvider,
    OrganiserProvider,
    VoyageProvider,
    HostsProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
