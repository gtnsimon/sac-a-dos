import {Component, ViewChild} from '@angular/core';
import { Storage } from '@ionic/storage';
import {Platform, Nav, ToastController, Menu, App} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginSlides } from "../pages/login-slides/login-slides";
import {TabsPage} from "../pages/tabs/tabs";
import {OrganiserPage} from "../pages/organiser/organiser";

/**
 * Met en majuscule la 1ère lettre seulement d'une chaine.
 * @param string
 */
export function ucfirst(string:string):string {
  return ((string.charAt(0)).toUpperCase() + string.slice(1));
}

/**
  * Récupère l'icone correspond à la ligne.
  * @param mode string Mode de transport (métro, train, ...)
  * @param id string
  */
export function lineIcon(mode:string, id:string = "") {
  let path:string = mode.toLowerCase();
  if(mode.toLowerCase() != "bus" && id != "") {
    id = id.split(':')[1];
    path += id;
  }
  return 'assets/sprites/' + path + '.png';
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  /**
   * Récupération du menu pour intéragir avec.
   */
  @ViewChild(Menu) menu: Menu;

  /**
   * Récupération de la navigation pour changer de page.
   */
  @ViewChild(Nav) navCtrl: Nav;

  /**
   * Page par défaut au démarrage de l'application.
   * @type {LoginSlides}
   */
  rootPage:any = LoginSlides;

  /**
   * Informations utilisateur.
   * @type {{}}
   */
  user:any = {};

  constructor(
    private app: App,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
    public toastCtrl: ToastController
  ) {
    this.initializeApp();
  }

  /**
   * @link https://github.com/driftyco/ionic2-starter-sidemenu/blob/master/src/app/app.component.ts
   */
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.backgroundColorByHexString('#07745e');
      this.splashScreen.hide();

      // https://forum.ionicframework.com/t/ionic2-global-navigation-change-event/51537/2
      this.navCtrl.viewWillEnter.subscribe(() => {
        this.loadUser();
      });
    });
  }

  /**
   * Récupération des informations de l'utilisateur.
   */
  loadUser() {
    this.storage.get('user').then((user) => {
      if(user !== null) {
        this.user = user;
        this.menu.enable(true);
      } else {
        this.menu.enable(false);
      }
    });
  }

  /**
   * Déconnexion.
   */
  logout() {
    // Message affiché lorsque déconnecté.
    let logout = this.toastCtrl.create({
      message: this.user.name + ", à bientôt !",
      position: 'top',
      duration: 3000,
      cssClass: 'toast toast-primary'
    });
    logout.present();

    this.storage.remove('user');
    this.navCtrl.setRoot(LoginSlides);

    console.info('LoginSlides', "Déconnexion");
  }

  /**
   * Navigation entre les onglets.
   * @param index
   */
  selectTab(index:number) {
    // https://forum.ionicframework.com/t/programatically-change-active-tab-on-ionic-2/45670/24
    this.app.getRootNav().getActiveChildNav().select(index);
  }

}
