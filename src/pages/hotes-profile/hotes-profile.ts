import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

@Component({
  selector: 'page-hotes-profile',
  templateUrl: 'hotes-profile.html',
})
export class HotesProfile {

  public host: any = {};
  public user_coords: any = {};
  public gmapHref: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.user_coords = this.navParams.get('user_coords');
    this.host = this.navParams.get('host');
  }

  ionViewDidLoad() {
    let coords = this.user_coords.lat + ',' + this.user_coords.lng;
    let host_coords = this.host.coords.lat + ',' + this.host.coords.lng;
    this.gmapHref = "https://www.google.com/maps/dir/" + coords + "/" + this.host.address + "/@" + host_coords + "/";
  }

  calcAge(date:string) {
    let now = new Date();
    let old = new Date(date);
    let age = now.getFullYear() - old.getFullYear();
    if(old.getUTCMonth() < now.getUTCMonth() && old.getUTCDay() < now.getUTCDay()) {
      age--;
    }
    return age;
  }

}
