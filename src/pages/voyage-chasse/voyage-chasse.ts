import {Component} from '@angular/core';
import {AlertController, LoadingController, ModalController, NavController, NavParams} from 'ionic-angular';
import {InfoStep} from "../voyage-map/info-step";
import {VoyageProvider} from "../../providers/voyage-provider";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {VoyageMap} from "../voyage-map/voyage-map";
import {Storage} from "@ionic/storage";
import {HotesMap} from "../hotes-map/hotes-map";

@Component({
  selector: 'page-voyage-chasse',
  templateUrl: 'voyage-chasse.html'
})
export class VoyageChasse {

  private canLeave: boolean = false;
  public travel: any = {};
  public step: any = {};
  public indications = [];
  public timer:string;
  private intervalID:number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private barcodeScanner: BarcodeScanner,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private voyageProvider: VoyageProvider
  ) {
  }

  ionViewDidLoad() {
    this.travel = this.navParams.get('travel');
    this.step = this.navParams.get('step');
    let timer = document.getElementById('timer');
    this.intervalID = setInterval(() => {
      console.log('update', timer);
      if(timer !== null) {
        let myTime = timer.innerText;
        let ss = myTime.split(":");
        let dt = new Date();
        dt.setHours(parseInt(ss[0]));
        dt.setMinutes(parseInt(ss[1]));
        dt.setSeconds(parseInt(ss[2]));

        let dt2 = new Date(dt.valueOf() + 1000);
        let ts = dt2.toTimeString().split(" ")[0];
        timer.innerText = ts;
      }
    }, 1000);
  }

  ionViewDidLeave() {
    clearInterval(this.intervalID);
  }

  ionViewCanLeave(): boolean {
    return this.canLeave;
  }

  showInfo() {
    let modal = this.modalCtrl.create(InfoStep, { step: this.step });
    modal.present();
  }

  pushIndication() {
    let wait = this.loadingCtrl.create({
      content: "Recherche d'une indication...",
    });
    wait.present();
    let excluded = this.indications.map((tip) => {
      return tip.id;
    });
    this.voyageProvider.getTip(this.step.id, excluded).subscribe(tip => {
      wait.dismiss();
      if(tip === null) {
        if(this.indications.length === 0) {
          this.indications.push({ id: 0, text: "Aucune indication trouvée..." });
        }
        document.getElementById('pushIndication').setAttribute('disabled', 'true');
      } else {
        this.indications.push(tip);
      }
    });
  }

  popIndication(index:number) {
    let alert = this.alertCtrl.create({
      title: "Effacer l'indication",
      message: this.indications[index].text,
      buttons: [
        {
          text: "Effacer",
          cssClass: "text-danger",
          handler: () => {
            this.indications.splice(index, 1);
            document.getElementById('pushIndication').removeAttribute('disabled');
          }
        },
        {
          text: "Annuler"
        }
      ]
    });
    alert.present();
  }

  leave() {
    this.timer = document.getElementById('timer').innerText;
    let alert = this.alertCtrl.create({
      title: "Arrêter la chasse",
      enableBackdropDismiss: false,
      subTitle: "Êtes-vous sûr de vouloir arrêter maintenant ?",
      message: "L'arrêt de votre de chasse vous empêchera de découvrir la boîte... Vous pourrez par ailleurs choisir un hôte !",
      buttons: [
        {
          text: "Continuer la chasse",
          handler: () => {
            document.getElementById('timer').innerText = this.timer;
          }
        },
        {
          text: "Arrêter maintenant",
          cssClass: "text-danger",
          handler: () => {
            let wait = this.loadingCtrl.create({
              content: "Annulation en cours...",
              dismissOnPageChange: true
            });
            wait.present();
            this.storage.get('user').then(user => {
              this.voyageProvider.cancelStep(user.id).subscribe(() => {
                this.canLeave = true;
                this.navCtrl.setRoot(VoyageMap, { travel: this.travel, canceled: true }, { animate: true, direction: 'backward' });
              }, error => {
                let alert = this.alertCtrl.create({
                  title: "Annulation",
                  subTitle: "Une erreur est survenue...",
                  message: "L'étape n'a pas pu être annulée.",
                  buttons: ['Fermer']
                });
                alert.present();
              });
            });
          }
        }
      ]
    });
    alert.present();
  }

  scanQR() {
    this.timer = document.getElementById('timer').innerText;
    this.barcodeScanner.scan().then(result => {
      // l'utilisateur a validé le QRCode
      if(result.cancelled === false) {
        let wait = this.loadingCtrl.create({
          content: "Validation en cours..."
        });
        wait.present();
        this.storage.get('user').then(user => {
          this.voyageProvider.scanQR(user.id, result.text).subscribe((res:any) => {
            this.canLeave = true;
            let alert = this.alertCtrl.create({
              title: "Chasse terminée !",
              subTitle: "Votre chrono est de " + this.timer + " !",
              message: "Vous pouvez maintenant faire une nouvelle étape ou rencontrer l'un de nos hôtes !",
              enableBackdropDismiss: false,
              buttons: [
                {
                  text: "Rencontrer un hôte",
                  handler: () => {
                    this.navCtrl.setRoot(HotesMap, { travel: this.travel });
                  }
                },
                {
                  text: "Nouvelle étape",
                  cssClass: "text-dark",
                  handler: () => {
                    this.navCtrl.setRoot(VoyageMap, { travel: this.travel });
                  }
                }
              ]
            });
            wait.dismiss();
            alert.present();
          }, err => {
            let body = JSON.parse(err._body);
            let alert = this.alertCtrl.create({
              title: "Validation impossible",
              subTitle: "Une erreur est survenue...",
              message: body.error,
              buttons: [
                {
                  text: "Réessayer",
                  handler: () => {
                    document.getElementById('timer').innerText = this.timer;
                  }
                }
              ]
            });
            wait.dismiss();
            alert.present();
          });
        });
      }
    }, (error) => {
      if(error === "cordova_not_available") {
        this.canLeave = true;
        this.navCtrl.setRoot(HotesMap, { travel: this.travel });
      } else {
        let alert = this.alertCtrl.create({
          title: "Scan QRCode",
          subTitle: "Une erreur est survenue...",
          message: error,
          buttons: ["Fermer"]
        });
        alert.present();
      }
    });
  }

}
