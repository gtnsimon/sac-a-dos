import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import {AlertController, LoadingController, NavController, ToastController} from 'ionic-angular';

import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

import { TabsPage } from "../tabs/tabs";
import { LoginProvider } from "../../providers/login-provider";
import {AddInformationsUser} from "../user/add-informations-user/add-informations-user";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  FB_APP_ID:number = 1144508035658828;
  alertsMessage:any = {
    default:  {
      title: "Connexion",
      text: "La connexion avec vos identifiants sera prochainement disponible...",
    },
    google: {
      title: "Connexion Google",
      text: "La connexion avec Google sera prochainement disponible...",
    }
  }

  constructor(
    public navCtrl: NavController,
    public fbConnect: Facebook,
    public loginProvider: LoginProvider,
    public storage: Storage,
    public alertCtrl: AlertController,
    private loadingCtrl: LoadingController
  ) {
    this.fbConnect.browserInit(this.FB_APP_ID, "v2.8");
  }

  /**
   * Facebook Connect.
   * @link https://ionicthemes.com/tutorials/about/ionic2-facebook-login
   */
  fbLogin() {
    let wait = this.loadingCtrl.create({
      content: "Connexion à Facebook..."
    });
    wait.present();

    let permissions = new Array();
    permissions.push("public_profile"); // https://developers.facebook.com/docs/facebook-login/permissions/

    this.fbConnect.login(permissions).then((res: FacebookLoginResponse):any => {
      let params = new Array();

      // Récupération des informations auprès de l'API Graph Facebook
      return this.fbConnect.api("/me?fields=email,name,first_name,last_name,cover,gender", params).then((data) => {
        let userPicture = "https://graph.facebook.com/" + data.id + "/picture?type=large";
        let userCover = data.cover.source;
        data.picture = userPicture;
        data.cover = userCover;
        return data;
      });
    }).then((fb_user) => {
      this.loginProvider.getUser(fb_user.id + "?fb").subscribe(bdd_user => {
        wait.dismiss();
        this.storage.set('user', bdd_user);
        this.navCtrl.setRoot(TabsPage, {}, { animate: true });

        console.info('fb_connect', "Success");
      }, error => {
        wait.dismiss();
        this.navCtrl.push(AddInformationsUser, { user: fb_user });
        console.error('fb_connect', error);
      });
    }).catch((error) => {
      wait.dismiss();
      // on utilise le compte de développement si cordova n'est pas dispo'
      if(error == "cordova_not_available") {
        this.loginProvider.getUser("163123937551428?fb").subscribe(bdd_user => {
          this.storage.set('user', bdd_user);
          this.navCtrl.setRoot(TabsPage, {}, { animate: true });

          console.info('fb_connect', "Utilisation du compte développement.");
        }, error => {
          let dev_user = {
            id: 163123937551428,
            name: "Dave Acompt",
            first_name: "Dave",
            last_name: "Acompt",
            gender: "male",
            picture: "assets/img/avatar-unknow.jpg",
            cover: "assets/img/card-amsterdam.png",
            email: "gaetan.simon@ens.uvsq.fr",
            mobile: "0601020304",
            address: "10 rue des poteries 78160"
          }
          this.navCtrl.push(AddInformationsUser, { user: dev_user });
          console.info('fb_connect', "Inscription du compte développement.");
        });
      } else {
        let alert = this.alertCtrl.create({
          title: "Connexion impossible",
          message: "Nous n'avons pas réussi à vous connecter...",
          buttons: ['Fermer']
        });
        alert.present();
        console.error('fb_connect', error);
      }
    });
  }

  showAlert(type: string) {
    let message = this.alertsMessage[type];
    let alert = this.alertCtrl.create({
      title: message.title + ":",
      subTitle: "<br>" + message.text,
      buttons: ['OK']
    });
    alert.present();
  }

}
