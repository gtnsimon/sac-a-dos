import {Component, ElementRef, ViewChild} from '@angular/core';
import {AlertController, LoadingController, ModalController, NavController, NavParams} from 'ionic-angular';
import {Geolocation} from "@ionic-native/geolocation";
import {VoyageProvider} from "../../providers/voyage-provider";
import {Storage} from "@ionic/storage";
import {InfoStep} from "./info-step";
import {LocalNotifications} from "@ionic-native/local-notifications";
import {VoyageChasse} from "../voyage-chasse/voyage-chasse";
import {HotesMap} from "../hotes-map/hotes-map";

declare var google;

const NOTIFICATION_ETAPE:number = 1;
const NOTIFICATION_CHASSE:number = 2;

@Component({
  selector: 'page-voyage-map',
  templateUrl: 'voyage-map.html',
})
export class VoyageMap {


  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('directionsPanel') directionsPanel: ElementRef;
  map: any;

  private canLeave: boolean = false;
  public mode: string = 'WALKING';
  public coords:any = null;
  public user:any = {};
  public step:any = {};
  public travel:any = {};
  public gmapHref:string;

  public directionsService:any;
  public directionsDisplay:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public geolocation: Geolocation,
    private localNotifications: LocalNotifications,
    public voyageProvider: VoyageProvider,
    private modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
  ) {
  }

  ionViewDidLoad() {
    this.travel = this.navParams.get('travel');
    console.log(this.navParams.get('canceled'));
    if(this.navParams.get('canceled') === true) {
      let alert = this.alertCtrl.create({
        title: "Chasse arrêtée",
        subTitle: "Un coup de fatigue ?",
        message: "Vous venez d'arrêter votre chasse... souhaiteriez-vous rencontrer un hôte ?",
        enableBackdropDismiss: false,
        buttons: [
          {
            text: "Oui, rencontrer un hôte",
            handler: () => {
              this.step.id = -1;
              this.canLeave = true;
              this.navCtrl.setRoot(HotesMap, { travel: this.travel }, {animate: true});
            }
          },
          {
            text: "Nouvelle étape",
            cssClass: "text-dark",
            handler: () => {
              this.initMap();
            }
          }
        ]
      });
      alert.present();
    } else {
      this.initMap();
    }
  }

  ionViewCanLeave(): boolean {
    return (this.step.id !== undefined) && this.canLeave;
  }

  initMap() {
    let wait = this.loadingCtrl.create({
      content: "Récupération du parcours..."
    });
    if(this.coords === null) {
      wait.present();
      this.geolocation.getCurrentPosition().then(pos => {
        this.coords = {lat: pos.coords.latitude, lng: pos.coords.longitude};
        //this.coords = {lat: 48.8745185, lng: 2.0827547};
        let user_coords = this.coords.lat + ',' + this.coords.lng;

        let setDistanceTrajet = function (service, mode: string, destination: string) {
          let id = 'trajet' + mode.toUpperCase();
          let callback = function (response, status) {
            if(response.rows[0].elements[0].status === 'OK') {
              let distance = response.rows[0].elements[0].distance.text ;
              let duration = response.rows[0].elements[0].duration.text;
              document.getElementById(id).innerText = distance + " - " + duration;
            } else {
              document.getElementById(id).innerText = "Itinéraire impossible";
            }
          }
          service.getDistanceMatrix({
            origins: [user_coords],
            destinations: [destination],
            travelMode: mode,
          }, callback);
        }

        this.directionsDisplay = new google.maps.DirectionsRenderer;
        this.directionsService = new google.maps.DirectionsService;
        let map = new google.maps.Map(this.mapElement.nativeElement, {
          zoom: 4,
          center: this.coords
        });
        this.directionsDisplay.setMap(map);
        this.directionsDisplay.setPanel(this.directionsPanel.nativeElement);

        this.storage.get('user').then(user => {
          this.user = user;
          let user_coords = user.address_coords.lat + ',' + user.address_coords.lng;
          this.voyageProvider.getTravelStep(user.id).subscribe(step => {
            this.step = step;
            let step_coords = step.coords.lat + ',' + step.coords.lng;
            this.gmapHref = "https://www.google.com/maps/dir/" + user_coords + "/" + step.name + ', ' + step.address + "/@" + step_coords;

            // nom de l'étape
            document.getElementById('stepName').innerText = step.name;
            // ville de l'étape
            document.getElementById('stepCity').innerText = step.city + ' - ' + step.zip_code;

            let service = new google.maps.DistanceMatrixService();

            setDistanceTrajet(service, 'WALKING', step_coords);
            setDistanceTrajet(service, 'BICYCLING', step_coords);
            setDistanceTrajet(service, 'TRANSIT', step_coords);

            this.calculateAndDisplayRoute(this.directionsService, this.directionsDisplay);
            wait.dismiss();

            this.localNotifications.isPresent(NOTIFICATION_CHASSE).then(() => {
              this.localNotifications.cancel(NOTIFICATION_CHASSE);
            }, err => console.error('localNotification', err));
            this.localNotifications.schedule({
              id: NOTIFICATION_ETAPE,
              title: "Une nouvelle étape vous attend !",
              text: this.step.name,
              icon: "file://assets/icon/notifications_icon.png",
              smallIcon: "res://ic_action_icon_notification",
              led: '00FF00'
            });
          }, res => {
            wait.dismiss();
            let body = JSON.parse(res._body);
            let alert = this.alertCtrl.create({
              title: "Étape du jour",
              message: body.error,
              buttons: [
                {
                  text: "Ooops",
                  cssClass: 'text-danger'
                }
              ]
            });
            alert.present();
            this.step.id = -1;
            this.canLeave = true;
            this.navCtrl.parent.select(0);
          });
        });
      }, error => {
        let alert = this.alertCtrl.create({
          title: "Localisation",
          subTitle: "Impossible de déterminer votre position.",
          message: "Nous n'avons pas réussi à déterminer votre position...",
          buttons: ['Fermer']
        });
        alert.present();
        console.error(error);
      });
    }
  }

  calculateAndDisplayRoute(directionsService, directionsDisplay) {
    directionsService.route({
      origin: this.coords,
      destination: this.step.coords,
      // Note that Javascript allows us to access the constant
      // using square brackets and a string value as its
      // "property."
      travelMode: google.maps.TravelMode[this.mode]
    }, function(response, status) {
      if (status == 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

  changeMode(mode: string) {
    this.mode = mode;
    this.calculateAndDisplayRoute(this.directionsService, this.directionsDisplay);
  }

  showInfo() {
    let modal = this.modalCtrl.create(InfoStep, { step: this.step });
    modal.present();
  }

  goStepSearch() {
    let alert = this.alertCtrl.create({
      title: "Commencer la chasse",
      message: "Votre chasse sera ardue. Amusez-vous bien !",
      buttons: [
        {
          text: "Plus tard",
          cssClass: "text-dark"
        },
        {
          text: "Commencer !",
          handler: () => {
            if(this.step.id !== undefined) {
              this.localNotifications.schedule({
                id: NOTIFICATION_CHASSE,
                title: this.step.name,
                text: "Votre chasse a commencé !",
                icon: "file://assets/icon/notifications_icon.png",
                smallIcon: "res://ic_action_icon_notification",
                led: '00FF00'
              });
              this.canLeave = true;
              this.navCtrl.setRoot(VoyageChasse, {travel: this.travel, step: this.step}, {animate: true}).catch(err => {
                console.log(err);
              });
            }
          }
        }
      ]
    });
    alert.present();
  }

}
