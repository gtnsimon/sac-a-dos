import { Component } from '@angular/core';
import {NavParams, ViewController} from "ionic-angular";

@Component({
  templateUrl: 'infoStep.modal.html'
})
export class InfoStep {

  public step: any;
  public currentDate: Date;

  constructor(
    private navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.currentDate = new Date();
    this.step = this.navParams.get('step');
  }

  formatHeures(jour: string) {
    let heures = this.step.schedules[jour];

    if(heures.closed) { // fermé pour ce jour
      document.getElementById('horaires-' + jour).classList.add('text-danger');
      return "Fermé";
    } else if(heures.day) { // ouvert toute la journée
      let tmp = heures.day.split(',').map(this.mapHeures);
      return "de " + tmp[0] + " à " + tmp[1];
    } else if(heures.am && heures.pm) { // ouvert de ... à ... et de ... à ...
      let am = heures.am.split(',').map(this.mapHeures);
      let pm = heures.pm.split(',').map(this.mapHeures);
      return "de " + am[0] + " à " + am[1] + "<br>et de " + pm[0] + " à " + pm[1];
    } else {
      return "N/A";
    }
  }

  mapHeures(value: string) {
    let hms = value.split(':');
    return hms[0] + "h" + hms[1];
  }

}
