import {Component} from '@angular/core';

import { HomePage } from '../home/home';
import { OrganiserPage } from "../organiser/organiser";
import {ProfileUser} from "../user/profile-user/profile-user";
import {App} from "ionic-angular";
import {VoyageProvider} from "../../providers/voyage-provider";
import {Storage} from "@ionic/storage";
import {VoyageMap} from "../voyage-map/voyage-map";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tabHome = HomePage;
  tabPlan:any;
  tabProfile = ProfileUser;

  public hasTravel:any = null;
  public tabParams: any = null;

  constructor(
    private app: App,
    public storage: Storage,
    public voyageProvider: VoyageProvider
  ) {
  }

  ionViewDidLoad() {
    this.determineTabPlan(false);
  }

  determineTabPlan(select:boolean = true) {
    this.storage.get('user').then(user => {
      this.voyageProvider.getCurrentTravel(user.id).subscribe(travel => {
        if(travel === null) {
          this.hasTravel = null;
          this.tabPlan = OrganiserPage;
          console.info('current_travel', "aucun voyage en cours");
        } else {
          this.hasTravel = 1;
          this.tabPlan = VoyageMap;
          this.tabParams = { travel: travel };
          console.log('current_travel', travel);
        }
      }, error => {
        console.error('next_travel', error);
      });
    });
  }

}
