import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from "../login/login";
import {Storage} from "@ionic/storage";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the LoginSlides page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-login-slides',
  templateUrl: 'login-slides.html',
})
export class LoginSlides {

  constructor(public navCtrl: NavController, public storage: Storage) {
  }

  ionViewWillEnter() {
    console.info('LoginSlides');
    this.storage.get('user').then(user => {
      if(user !== null) {
        this.navCtrl.setRoot(TabsPage, {}, { animate: true });
        console.info('user', "Utilisation du LocalStorage.");
      }
    });
  }

  openLoginPage() {
    this.navCtrl.push(LoginPage);
  }

}
