import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-step-two-organiser',
  templateUrl: 'step-two-organiser.html',
})
export class StepTwoOrganiser {

  private travelOptions: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.travelOptions = this.navParams.get('travelOptions');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StepTwoOrganiser');
    console.log(this.travelOptions);
  }

}
