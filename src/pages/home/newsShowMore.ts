import { Component } from '@angular/core';
import {App, Loading, LoadingController, NavParams, ViewController} from "ionic-angular";
import { lineIcon } from "../../app/app.component";
import { InfoTrafic } from "../../providers/info-trafic";

// https://github.com/mozilla/source-map/issues/247

@Component({
  templateUrl: 'newsShowMore.modal.html'
})
export class NewsShowMore {

    line:any;
    lines_details: any[];

    private wait: Loading;

    // https://github.com/driftyco/ionic-preview-app/blob/master/src/pages/modals/basic/pages.ts
    constructor(
      private params: NavParams,
      public app: App,
      public viewCtrl: ViewController,
      public infoTrafic: InfoTrafic,
      private loadingCtrl: LoadingController
    ) {
      this.line = this.params.get('line');
    }

    ionViewDidEnter() {
      this.wait.dismiss();
    }

    ionViewDidLoad() {
      this.wait = this.loadingCtrl.create({
        content: "Récupération de l'infotrafic..."
      });
      this.wait.present();
      this.getLinesInfos();
    }

    lineIcon(mode:string, id:string = "") {
        return lineIcon(mode, id);
    }

    getLinesInfos() {
      let lines:any = this.line.lines;
      let lines_details = [];
      lines.forEach(line => {
          if(lines_details[line.id] === undefined) {
              this.infoTrafic.getInfoTrafic(line.id).subscribe((data) => {
                  lines_details.push({
                      id: line.id,
                      news: data
                  });
              });
          }
      });
      this.lines_details = lines_details;
      console.log('trafic:details ' + this.line.mode, lines_details);
    }

    getSeverity(level:number) {
        return 'assets/sprites/severity' + level + '.png';
    }

    newsPlural(count:number) {
        return count + " pertubation" + ((count > 1) ? 's' : '');
    }

}
