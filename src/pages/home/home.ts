import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import {AlertController, LoadingController, ModalController} from 'ionic-angular';
import { InfoTrafic } from "../../providers/info-trafic";
import { ucfirst, lineIcon } from "../../app/app.component";
import { NewsShowMore } from "./newsShowMore";
import { Meteo } from "../../providers/meteo";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  /**
   * Information trafic.
   * @type {any}
   */
  trafic:any = undefined;

  /**
   * Information météo.
   * @type {any}
   */
  meteo:any  = {};

  defaultInfo:string = "";

  constructor(
    private geolocation: Geolocation,
    public modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public infoTrafic: InfoTrafic,
    public meteoAPI: Meteo,
  ) {
  }

  ionViewDidLoad() {
    let wait = this.loadingCtrl.create({
      content: "Préparation...",
      duration: 5000
    });
    wait.present();

    // Récupération de l'info-trafic.
    this.infoTrafic.getInfoTrafic().subscribe((data) => {
      this.trafic = data;
      this.defaultInfo = data[0].mode.toLowerCase();
      console.log('trafic:direct', data);
    }, err => {
      wait.dismiss();
      let alert = this.alertCtrl.create({
        title: "Infotrafic",
        subTitle: "Impossible de charger l'infotrafic...",
        buttons: ['Fermer']
      });
      alert.present();
    });

    // Récupération de la météo.
    this.geolocation.getCurrentPosition().then((res) => {
      this.meteoAPI.getWeatherByPos(res.coords.latitude, res.coords.longitude).subscribe((data) => {
        data.main.temp = Math.round(data.main.temp);
        data.wind.speed = Math.round(data.wind.speed * 3.6);
        this.meteo = data;
        console.log('meteo', data);
        wait.dismiss();
      }, err => {
        wait.dismiss();
        let alert = this.alertCtrl.create({
          title: "Météo",
          subTitle: "Impossible de récupérer la météo...",
          buttons: ['Fermer']
        });
        alert.present();
      });
    }, error => {
      wait.dismiss();
      let alert = this.alertCtrl.create({
        title: "Géolocalisation",
        subTitle: "Impossible de récupérer  votre position...",
        buttons: ['Fermer']
      });
      alert.present();
    });
  }

  /**
   * @param mode
   * @param id
   * @returns {string}
   */
  lineIcon(mode:string, id:string = "") {
      return lineIcon(mode, id);
  }

  /**
   * Coupe le texte de l'info-trafic pour ne pas afficher le nom de la ligne.
   * @param newsLabel string
   */
  newsText(newsLabel:string) {
    newsLabel = newsLabel.trim();
    let split = newsLabel.split(':');
    newsLabel = (split.length == 2 && split[1].length > 3) ? split[1] : newsLabel;
    return ucfirst(newsLabel);
  }

  /**
   * Texte du bouton pour afficher plus de perturbations.
   * @param length
   * @returns {string}
   */
  newsCountMore(length:number) {
    let more = length - 3;
    let plural = "perturbation" + ((more > 1) ? 's' : '');
    return more + " " + plural;
  }

  /**
   * Affichage détaillé de l'info-trafic par mode de transport.
   * @param line
   */
  newsShowMore(line:any) {
    let newsModal = this.modalCtrl.create(NewsShowMore, { line: line });
    newsModal.present();
  }

}
