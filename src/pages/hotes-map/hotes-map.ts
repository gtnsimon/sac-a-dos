import {Component, ElementRef, ViewChild} from '@angular/core';
import {AlertController, LoadingController, ModalController, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {Geolocation} from "@ionic-native/geolocation";
import {HotesProfile} from "../hotes-profile/hotes-profile";
import {HostsProvider} from "../../providers/hosts-provider";
import {VoyageMap} from "../voyage-map/voyage-map";

declare var google;

@Component({
  selector: 'page-hotes-map',
  templateUrl: 'hotes-map.html'
})
export class HotesMap {

  @ViewChild('map') mapElement: ElementRef;

  private travel: any = {};
  private user: any = {};
  private hosts: any = {};
  private userPosition: any = {};
  private canLeave:boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private geolocation: Geolocation,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private hostsProvider: HostsProvider
  ) {
  }

  ionViewDidLoad() {
    this.travel = this.navParams.get('travel');

    this.storage.get('user').then(user => {
      this.user = user;
      this.initMap();
    });
  }

  ionViewCanLeave():boolean {
    return this.canLeave;
  }

  initMap() {
    let wait = this.loadingCtrl.create({
      content: "Récupération des hôtes..."
    });
    wait.present();
    this.geolocation.getCurrentPosition().then(pos => {
      this.userPosition = { lat: pos.coords.latitude, lng: pos.coords.longitude };
      let userWindowContent = '<div id="content">'+
          '<div id="siteNotice"></div>'+
          '<h1 id="firstHeading" class="firstHeading">' + this.user.name + ' (moi)</h1>'+
          '</div>';

      let map = new google.maps.Map(this.mapElement.nativeElement, {
        zoom: 10,
        center: this.userPosition
      });

      let userWindow = new google.maps.InfoWindow({
        content: userWindowContent
      });
      let userMarker = new google.maps.Marker({
        map: map,
        position: this.userPosition,
      });
      userMarker.addListener('click', function () {
        userWindow.open(map, userMarker);
      });

      this.hostsProvider.getHostsNearBy(this.userPosition.lat, this.userPosition.lng).subscribe(res => {
        this.hosts = res;
        res.forEach(host => {
          let content = new google.maps.InfoWindow({
            content: '<div id="content">'+
            '<div id="siteNotice"></div>'+
            '<h1 id="firstHeading" class="firstHeading">' + host.first_name + ' ' + host.last_name.charAt(0) + '.</h1>'+
            '</div>'
          });
          let marker = new google.maps.Marker({
            map: map,
            position: host.coords,
            label: host.first_name.charAt(0)
          });
          marker.addListener('click', function () {
            content.open(map, marker);
          });
        });
        wait.dismiss();
      }, err => {
        wait.dismiss();
        let alert = this.alertCtrl.create({
          title: "Aucun hôte",
          subTitle: "Personne n'est à proximité...",
          message: "Malheureusement, aucun n'est à proximité de vous...",
          buttons: ["D'accord", "D'accord (mais triste)"]
        });
        alert.present();
      });
    })
  }

  viewProfile(host:any) {
    let modal = this.modalCtrl.create(HotesProfile, { host: host, user_coords: this.userPosition });
    modal.present();
  }

  nouvelleEtape() {
    let alert = this.alertCtrl.create({
      title: "Nouvelle étape",
      subTitle: "Vous n'êtes pas fatigué ?",
      message: "La lancement d'une nouvelle étape vous empêchera de trouver un hôte...",
      buttons: [
        {
          text: "Continuer la recherche"
        },
        {
          text: "Nouvelle étape",
          cssClass: "text-dark",
          handler: () => {
            this.canLeave = true;
            this.navCtrl.setRoot(VoyageMap, { travel: this.travel }, { animate: true, direction: 'backward' });
          }
        }
      ]
    });
    alert.present();
  }

  calcAge(date:string) {
    let now = new Date();
    let old = new Date(date);
    let age = now.getFullYear() - old.getFullYear();
    if(old.getUTCMonth() < now.getUTCMonth() && old.getUTCDay() < now.getUTCDay()) {
      age--;
    }
    return age;
  }

}
