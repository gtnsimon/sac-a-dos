import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import {AlertController, LoadingController, NavController, Tabs} from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {OrganiserProvider} from "../../providers/organiser-provider";
import {VoyageMap} from "../voyage-map/voyage-map";

@Component({
  selector: 'page-organiser',
  templateUrl: 'organiser.html',
})
export class OrganiserPage {

  public budget:number = 250;
  private travel_data: FormGroup;

  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    public organiserProvider: OrganiserProvider,
    private formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {
    let today: Date = new Date();
    let departure_at = new Date();

    departure_at.setDate(today.getDate() + 5);
    this.travel_data = this.formBuilder.group({
      arrival_at: [today.toISOString(), Validators.required],
      departure_at: [departure_at.toISOString(), Validators.required],
      budget: [this.budget, Validators.required],
      buddies: [1, Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrganiserPage');
  }

  budgetDollars(budget: number):number {
    return Math.round(budget * 1.08955);
  }

  cancel() {
    // https://forum.ionicframework.com/t/programatically-change-active-tab-on-ionic-2/45670/9
    this.navCtrl.parent.select(0);
  }

  alertDev() {
    let alert = this.alertCtrl.create({
      title: "Planification",
      subTitle: "Prochainement, composez votre chemin !",
      message: "Dans la prochaine mise à jour, vous pourrez sélectionner un parcours répondant le plus à vos centres d'intérêt.",
      buttons: [
        {
          text: "Annuler",
          role: 'cancel',
          cssClass: 'text-dark'
        },
        {
          text: "D'accord",
          handler: () => {
            this.submit();
          }
        }
      ]
    });
    alert.present();
  }

  submit() {
    let travelOptions = Object.assign(this.travel_data.value);
    let wait = this.loadingCtrl.create({
      content: "Création de votre voyage...",
      dismissOnPageChange: true
    });
    wait.present();
    this.storage.get('user').then(user => {
      travelOptions.user_id = user.id;
      travelOptions.activities = user.likes;
      this.organiserProvider.putTravel(travelOptions).subscribe(data => {
        this.navCtrl.setRoot(VoyageMap, { travel: data });
      }, error => {
        wait.dismiss();
        let alert = this.alertCtrl.create({
          title: "Erreur de traitement",
          subTitle: "Création de votre voyage impossible.",
          message: "Nous n'avons pas pu enregistrer votre voyage. Veuillez réessayer utlérieurement.",
          buttons: ['Réessayer plus tard']
        });
        alert.present();
      });
    })
  }

}
