import { Component } from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {LoginProvider} from "../../../providers/login-provider";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ProfileUser} from "../profile-user/profile-user";

/**
 * Generated class for the AddInformationsLikesUser page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-add-informations-likes-user',
  templateUrl: 'add-informations-likes-user.html',
})
export class AddInformationsLikesUser {

  public user:any;
  public likes: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public formBuilder: FormBuilder, public loginProvider: LoginProvider, public loadingCtrl: LoadingController) {
    this.user = this.navParams.get('user');
    this.initForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddInformationsLikesUser');
  }

  initForm() {
    // http://masteringionic.com/blog/2017-01-28-validating-multiple-checkboxes-with-ionic/?s=2017-01-28-validating-multiple-checkboxes-with-ionic-2/
    this.likes = this.formBuilder.group({
      'likes': this.formBuilder.group({
        nature: [false],
        histoire: [false],
        sport: [false],
        aventure: [false],
        gastronomie: [false],
      })
    });
  }

  submit() {
    let likes = this.likes.controls.likes.value;
    this.user.likes = likes;
    let wait = this.loadingCtrl.create({
      content: "Inscription en cours..."
    });
    wait.present();
    this.loginProvider.putUser(this.user).subscribe(data => {
      console.log(data);
      wait.dismiss();
      this.storage.set('user', data);
      this.navCtrl.setRoot(ProfileUser, { firstLogin: true }, { animate: true });
    }, error => {
      wait.dismiss();
      console.error(error);
    })
  }

}
