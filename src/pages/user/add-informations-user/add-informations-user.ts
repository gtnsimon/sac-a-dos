import { Component } from '@angular/core';
import {AlertController, NavController, NavParams} from 'ionic-angular';
import {AddInformationsLikesUser} from "../add-informations-likes-user/add-informations-likes-user";
import {LoginProvider} from "../../../providers/login-provider";

@Component({
  selector: 'page-add-informations-user',
  templateUrl: 'add-informations-user.html',
})
export class AddInformationsUser {

  public user:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loginProvider: LoginProvider, public alertCtrl: AlertController) {
    this.user = navParams.get('user');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddInformationsUser');
  }

  submitForm() {
    console.log(this.user);
    if(this.user.address === undefined || this.user.address.length < 20) {
      let alert = this.alertCtrl.create({
        title: "Veuillez corriger.",
        subTitle: "<br> Merci de compléter votre adresse postale.",
        buttons: ['OK']
      });
      alert.present();
    } else if(this.user.email === undefined || this.user.email.length < 10) {
      let alert = this.alertCtrl.create({
        title: "Veuillez corriger.",
        subTitle: "<br> Merci de compléter votre adresse e-mail.",
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.navCtrl.push(AddInformationsLikesUser, { user: this.user })
    }
  }

}
