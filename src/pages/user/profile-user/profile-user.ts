import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {TabsPage} from "../../tabs/tabs";
import {ucfirst} from "../../../app/app.component";

/**
 * Generated class for the ProfileUser page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-profile-user',
  templateUrl: 'profile-user.html',
})
export class ProfileUser {

  public user:any = {};
  public firstLogin:any;
  public formated_likes:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
  }

  ionViewDidLoad() {
    this.firstLogin = this.navParams.get('firstLogin');
    this.storage.get('user').then(user => {
      this.user = user;
      this.formated_likes = ucfirst(user.likes.join(', '));
      console.info('user', user);
    });
    console.log('ionViewDidLoad ProfileUser');
  }

  goHome() {
    this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: 'forward' });
  }

}
