Sac à dos
=========

Sac à dos est une application mobile d’aide à l’organisation de voyages version “backpack”.
Elle permet de découvrir les Yvelines (et, à terme, l’Europe) d'une façon différente et en donnant accès à des lieux cachés, loin du tourisme traditionnel.

## Commandes de base

- Lancer l'application dans le navigateur :

    ```bash
    ionic lab -b
    ```

- Lancer l'application sur le téléphone :

    ```bash
    ionic run android --device
    ```

## Plugins installés

- [Facebook Connect](https://ionicframework.com/docs/native/facebook/)

    **Intégration de Facebook Connect** :  
    https://ionicthemes.com/tutorials/about/ionic2-facebook-login

    ```bash
    ionic plugin add cordova-plugin-facebook4 --variable APP_ID="1144508035658828" --variable APP_NAME="sac-a-dos"
    ```

- [Geolocation](https://ionicframework.com/docs/native/geolocation/)

    **Géolocalisation de l'utilisateur**

    ```bash
    ionic plugin add --save cordova-plugin-geolocation
    npm install --save @ionic-native/geolocation
    ```

- [API OpenWeatherMap](https://openweathermap.org/api)

    Récupération de la météo en fonction des coordonnées GPS.
    
- [Local Notifications](https://ionicframework.com/docs/native/local-notifications/)

    ```bash
    ionic plugin add --save de.appplant.cordova.plugin.local-notification
    npm install --save @ionic-native/local-notifications
    ```
    
- [Barcode Scanner](http://ionicframework.com/docs/native/barcode-scanner/)

    ```bash
    ionic plugin add --save phonegap-plugin-barcodescanner
    npm install --save @ionic-native/barcode-scanner
    ```

- TYPES GOOGLEMAPS

    ```bash
    npm install --save @types/googlemaps
    ```
